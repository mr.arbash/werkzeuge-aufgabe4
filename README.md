# Werkzeuge für das wissenschaftliche Arbeiten

##### Salah Arbash
##### 694720

###### Lehrinhalte:
- Gute wissenschaftliche Praxis
- Versionierungssoftware git  
- LaTeX-Grundlagen und Erweiterungen
- Computer-Algebra-Systeme (Matlab, Mathematica, Maple)
- Softwaresysteme für statistische Analysen
- Recherche in elektronischen Bibliotheken (DBLP, ACM, IEEE)
- Verfassen einer wissenschaftlichen Arbeit

